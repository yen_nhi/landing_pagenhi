<div class="container-fil" style="margin-top:0px">
    <div class="row">
        <div class="col-sm-6">
            <img src="../../img/2.jpg" style="width: 100%; height: 100%">
        </div>
        <div class="col-sm-6">
            <h1 align="center">GIỚI THIỆU VỀ ITPLUS ACADEMY</h1>
            <p style="text-align: justify">ITPlus Academy được thành lập từ năm 2011 với nhiệm vụ nghiên cứu, ứng dụng khoa
                học công nghệ đào tạo nhân lực chất lượng cao ngành Công nghệ thông tin. Dựa vào
                nhu cầu thực tế của các Tập đoàn Công nghệ thông tin, ITPlus Academy hợp tác với Học
                viện Công nghệ Bưu chính Viễn thông, Đại Học Sân Khấu Điện Ảnh triển khai chương
                trình đào tạo chuyên sâu với các chuyên ngành: Lập trình ứng dụng và Thiết kế đồ họa
                – Truyền thông Đa phương tiện.
                Sau 9 năm phát triển ITPlus Academy đã đào tạo hơn 10.000 sinh viên, hợp tác với hơn
                75 doanh nghiệp hàng đầu trong lĩnh vực công nghệ thông tin và thiết kế đồ họa như:
                Samsung, LG, FPT Software, Viettel, VNPT, CMC, VCCorp, Netnam, Tinh Vân, VNG,
                Gameloft, Esoftflow, Telsoft, Izisolution, Ecommage, Vnext, EcoIT…. mang đến cơ hội việc
                làm rộng mở cho sinh viên ngay sau khi tốt nghiệp.
                Các chương trình đào tạo của ITPlus Academy được nhận "Giải thưởng Sao Khuê 2017,
                2018 & 2019" cho các sản phẩm, dịch vụ xuất sắc nhất ngành phần mềm và dịch vụ
                Công Nghệ Thông Tin của Việt Nam. Và "Giải thưởng Chuyển đổi số 2019" cho sản
                phẩm, dịch vụ góp phần vào công cuộc Chuyển đổi số Quốc gia.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <h1 align="center">ĐỐI TƯỢNG KHÓA HỌC</h1>
           <h3>Sinh viên CNTT</h3>
            <p>Muốn học bài bản, có khả năng thực chiến tốt với PHP.</p>
            <h3>Lập trình viên</h3>
            <p>Đang làm việc tại các Doanh nghiệp, muốn bổ sung kiến thức và kỹ năng lập trình PHP
                để phục vụ cho công việc.</p>
            <h3>Học sinh, sinh viên, người đi làm</h3>
            <p>Mới bắt đầu tìm hiểu về lập trình.</p>
        </div>
        <div class="col-sm-6">
            <img src="../../img/3.jpg" style="width: 100%; height: 100%">
        </div>
    </div>
</div>
<div class="container-fil" style="margin-top:0px">
    <img class="banner2" src="../../img/4.jpg" style="width: 100%; height: 100%">
</div>
<div class="container-fil" style="margin-top:0px;background-color: #cacaca">
    <h1 style="padding: 30px"> CÁC KHÓA HỌC LẬP TRÌNH</h1>
    <div class="row">
    <div class="col-sm-4">
        <div class="container">
            <div class="card" style="width:100%">
                <img class="card-img-top" src="../../img/khao%20hoc-01.jpg" alt="Card image" style="width:100%">
                <div class="card-body">
                    <h4 class="card-title">John Doe</h4>
                    <p class="card-text">Some example text some example text. John Doe is an architect and engineer</p>
                    <a href="#" class="btn btn-primary">See Profile</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="container">
            <div class="card" style="width:100%">
                <img class="card-img-top" src="../../img/khao%20hoc-02.jpg" alt="Card image" style="width:100%">
                <div class="card-body">
                    <h4 class="card-title">John Doe</h4>
                    <p class="card-text">Some example text some example text. John Doe is an architect and engineer</p>
                    <a href="#" class="btn btn-primary">See Profile</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="container">
            <div class="card" style="width:100%">
                <img class="card-img-top" src="../../img/khao%20hoc-03.jpg" alt="Card image" style="width:100%">
                <div class="card-body">
                    <h4 class="card-title">John Doe</h4>
                    <p class="card-text">Some example text some example text. John Doe is an architect and engineer</p>
                    <a href="#" class="btn btn-primary">See Profile</a>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="container">
                <div class="card" style="width:100%">
                    <img class="card-img-top" src="../../img/khao%20hoc-04.jpg" alt="Card image" style="width:100%">
                    <div class="card-body">
                        <h4 class="card-title">John Doe</h4>
                        <p class="card-text">Some example text some example text. John Doe is an architect and engineer</p>
                        <a href="#" class="btn btn-primary">See Profile</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="container">
                <div class="card" style="width:100%">
                    <img class="card-img-top" src="../../img/khao%20hoc-05.jpg" alt="Card image" style="width:100%">
                    <div class="card-body">
                        <h4 class="card-title">John Doe</h4>
                        <p class="card-text">Some example text some example text. John Doe is an architect and engineer</p>
                        <a href="#" class="btn btn-primary">See Profile</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="container">
                <div class="card" style="width:100%">
                    <img class="card-img-top" src="../../img/khao%20hoc-06.jpg" alt="Card image" style="width:100%">
                    <div class="card-body">
                        <h4 class="card-title">John Doe</h4>
                        <p class="card-text">Some example text some example text. John Doe is an architect and engineer</p>
                        <a href="#" class="btn btn-primary">See Profile</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fil" style="margin-top:0px;background-color: #cacaca">
    <h1 style="padding: 30px">ĐỘI NGŨ GIẢNG VIÊN </h1>
    <div class="container-fil" style=" margin: 20px;margin-bottom:30px">
        <div class="row">
            <div class="col-sm-2">
                <img src="../../img/giang%20vien-01.jpg" style="width: 100%; height: 100%">
            </div>
            <div class="col-sm-2">
                <img src="../../img/giang%20vien-02.jpg" style="width: 100%; height: 100%">
            </div>
            <div class="col-sm-2">
                <img src="../../img/giang%20vien-03.jpg" style="width: 100%; height: 100%">
            </div>
            <div class="col-sm-2">
                <img src="../../img/giang%20vien-04.jpg" style="width: 100%; height: 100%">
            </div>
            <div class="col-sm-2">
                <img src="../../img/giang%20vien-05.jpg" style="width: 100%; height: 100%">
            </div>
            <div class="col-sm-2">
            <img src="../../img/giang%20vien-06.jpg" style="width: 100%; height: 100%">
        </div>
        </div>
        <div class="row" style="padding-top: 30px">
            <div class="col-sm-2">
                <img src="../../img/giang%20vien-07.jpg" style="width: 100%; height: 100%">
            </div>
            <div class="col-sm-2">
                <img src="../../img/giang%20vien-08.jpg" style="width: 100%; height: 100%">
            </div>
            <div class="col-sm-2">
                <img src="../../img/giang%20vien-09.jpg" style="width: 100%; height: 100%">
            </div>
            <div class="col-sm-2">
                <img src="../../img/giang%20vien-10.jpg" style="width: 100%; height: 100%">
            </div>
            <div class="col-sm-2">
                <img src="../../img/giang%20vien-11.jpg" style="width: 100%; height: 100%">
            </div>
            <div class="col-sm-2">
                <img src="../../img/giang%20vien-12.jpg" style="width: 100%; height: 100%">
            </div>
        </div>
    </div>
</div>

<div class="container-fil" style="margin-top:0px;background-color: #2A2C2B">
<h1 style="padding: 30px;color: white" align="center">SẢN PHẨM HỌC VIÊN </h1>
        <div class="multiple-items" >
            <div><img src="../../img/sanphamhocvien01.jpg" style="width: 500px;height:500px;padding: 30px;margin:60px"></div>
            <div><img src="../../img/sanphamhocvien02.jpg" style="width: 500px;height:500px;padding: 30px;margin:60px"></div>
            <div><img src="../../img/sanphamhocvien03.png" style="width: 500px;height:500px;padding: 30px;margin:60px"></div>
            <div><img src="../../img/sanphamhocvien04.jpg" style="width: 500px;height:500px;padding: 30px;margin:60px"></div>
            <div><img src="../../img/sanphamhocvien05.jpg" style="width: 500px;height:500px;padding: 30px;margin:60px"></div>
            <div><img src="../../img/sanphamhocvien01.jpg" style="width: 500px;height:500px;padding: 30px;margin:60px"></div>
        </div>
</div>

<div class="container-fil" style="margin-top:0px;background-color: #cacaca">
    <h1 align="center" style="padding: 30px">CƠ SỞ VẬT CHẤT  </h1>
    <div class="container-fil6" style=" margin: 20px;margin-bottom:30px">
        <div class="coso">
            <div>
                <img src="../../img/csvc1.jpg" style="width: 350px; height: 350px; padding: 30px">
            </div>
            <div>
                <img src="../../img/csvc2.jpg" style="width: 350px; height: 350px; padding: 30px">
            </div>
            <div>
                <img src="../../img/csvc3.jpg" style="width: 350px; height: 350px; padding: 30px">
            </div>
            <div>
                <img src="../../img/csvc4.jpg" style="width: 350px; height: 350px; padding: 30px">
            </div>
            <div>
                <img src="../../img/csvc5.jpg" style="width: 350px; height: 350px; padding: 30px">
            </div>
            <div>
                <img src="../../img/csvc1.jpg" style="width: 350px; height: 350px; padding: 30px">
            </div>
            <div>
                <img src="../../img/csvc2.jpg" style="width: 350px; height: 350px; padding: 30px">
            </div>
        </div>
    </div>
</div>
<div class="container-fil" style="margin-top:0px;background-color: #2A2C2B">
    <h1 style="padding: 30px;color: white" align="center">MÔI TRƯỜNG HỌC TẬP NĂNG ĐỘNG </h1>
    <div class="moitruong" >
        <div>
            <img src="../../img/moitruong1.jpg" style="width: 350px; height: 350px; padding: 30px">
        </div>
        <div>
            <img src="../../img/moitruong2.jpg" style="width: 350px; height: 350px; padding: 30px">
        </div>
        <div>
            <img src="../../img/moitruong3.jpg" style="width: 350px; height: 350px; padding: 30px">
        </div>
        <div>
            <img src="../../img/moitruong4.jpeg" style="width: 350px; height: 350px; padding: 30px">
        </div>
        <div>
            <img src="../../img/moitruong6.png" style="width: 350px; height: 350px; padding: 30px">
        </div>
        <div>
            <img src="../../img/moitruong5.jpg" style="width: 350px; height: 350px; padding: 30px">
        </div>
        <div>
            <img src="../../img/moitruong7.png" style="width: 350px; height: 350px; padding: 30px">
        </div>
    </div>
</div>

<div class="container-fil" style="margin-top:30px">
    <img class="banner2" src="../../img/lichkhaigiang.PNG" style="width: 100%; height: 100%">
</div>



